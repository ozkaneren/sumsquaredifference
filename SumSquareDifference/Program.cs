﻿using System;

namespace SumSquareDifference
{
    public class Program
    {
        static void Main(string[] args)
        {
            //The sum of the squares of the first ten natural numbers is,

            //12 + 22 + ... + 102 = 385
            //The square of the sum of the first ten natural numbers is,

            //(1 + 2 + ... + 10)2 = 552 = 3025
            //Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

            //Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.


            int theLastNumber = 100;

            int totalFirstOneHundred = theLastNumber * (theLastNumber + 1) * (theLastNumber * 2 + 1) / 6;

            int squarestotalFirstOneHundred =
                theLastNumber * (theLastNumber + 1) / 2 * (theLastNumber * (theLastNumber + 1)) / 2;

            int different = squarestotalFirstOneHundred - totalFirstOneHundred;

            Console.WriteLine(different);

            Console.Read();
        }
    }
}
